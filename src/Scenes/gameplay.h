#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include <math.h>
#include "raylib.h"
#include "ExternV/entities_gameplay.h"
#include "ExternV/general_variables.h"
#include "controler/game.h"
#include "gameplay/gameplayLogic.h"
#include "both/posFuns.h"
//el arkanoid original cuenta con 13 bloques de largo y 6 de alto en su primer nivel.

#define PLAYER_MAX_LIFE 4
#define FPSGROUND 45;
#define FPSPLAYER 15;

namespace gameplay
{
	extern short level;
	extern short score;
	extern int totalscore;
	namespace animatedTimer
	{
		extern short player;        //player time change de anim.
		extern short ground;        //ground time change de anim.

	}
	namespace entities
	{
		extern Ball ball;
		extern Player player;
		extern Brick bricks[LINES_OF_BRICKS][BRICKS_PER_LINE];
		extern Vector2 brickSize;
		namespace ground
		{
			extern Texture2D texture;
			extern int frameHightGround;
			extern Rectangle groundSource;
			extern Rectangle groundBounds;
			extern short groundState;
			extern Sound sfall;
			extern Sound swalls;
			extern Music sground;
		}
		namespace uses_bricks
		{
			extern int frameHightBlock;
			extern Sound bricks_crack;
			extern Texture2D bricks_texture;
			extern Vector2 brickSize;
		}
	}
	namespace logic
	{
		extern bool gameOver;
		extern bool pause;
		extern bool mouse;
		extern bool win;
	}
	
	extern void init();
	extern void update();
	extern void draw();
	extern void deinit();
}
#endif