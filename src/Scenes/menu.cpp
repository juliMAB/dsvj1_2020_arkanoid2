#include "menu.h"
#include <iostream>
#define quantity_buttons 13
//#include "controler/game.h"

namespace menu
{
	//botones
	button btn[quantity_buttons];
	enum class Bts
	{
		play,
		exit,
		options,
		credits,
		back,
		next,
		mas,
		menos,
		barras,
		mas2,
		menos2,
		barras2,

	};
	//mouse
	Vector2 mousePoint;
	//sonido para botones
	Sound fxButton;
	//fondo
	Texture2D ground;
	//musica del menu
	Music music;
	//credits
	Texture2D credits;
	
	
	namespace scenes
	{
		
		menu_scenes actual_menu_scenes;
		
		options ops;
	}
	
	
	void init()
	{
		//iniciar Scene.
		scenes::actual_menu_scenes = scenes::menu_scenes::intro;
		//iniciar audios.
		menuLogic::init::chargeAudios();
		//iniciar botones.
		menuLogic::init::allBUTTONS(quantity_buttons,btn);
		//iniciar Musica.
		menuLogic::init::chargeMusic();
		
	}
	void update()
	{

		UpdateMusicStream(music);
		mousePoint = GetMousePosition();
		if (!IsMusicPlaying(music))
		{
			PlayMusicStream(music);
		}
		switch (scenes::actual_menu_scenes)
		{

		case menu::scenes::menu_scenes::general:

			menuLogic::update::updateGeneral(btn);

			break;

		case menu::scenes::menu_scenes::intro:

			if (GetKeyPressed() || IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			{
				menu::scenes::actual_menu_scenes = menu::scenes::menu_scenes::general;
			}

			break;

		case menu::scenes::menu_scenes::options:

			switch (menu::scenes::ops)
			{
			case menu::scenes::options::one:
				menuLogic::update::updateOptions(btn);
				break;
			case menu::scenes::options::two:
				menuLogic::update::updateOptions2(btn);
				break;
			}
			

			break;
		case menu::scenes::menu_scenes::credits:
		//boton back.
		{
			if (CheckCollisionPointRec(mousePoint, btn[4].bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) btn[4].state = 2;
				else btn[4].state = 1;
				if (!btn[4].touch)
				{
					PlaySound(fxButton);
					btn[4].touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					btn[4].action = true;

					menu::scenes::actual_menu_scenes = menu::scenes::menu_scenes::general;
				}
			}
			else
			{
				btn[4].touch = false;
				btn[4].state = 0;
			}

			btn[4].sourceRec.y = (float)btn[4].state * (float)btn[4].frameHeight;
		}
		default:
			break;
		}
		for (short i = 0; i < quantity_buttons; i++)
		{
			btn[i].action = false;
		}

		
	}

	void draw()
	{
		DrawTexture(ground, 0, 0, WHITE);
		switch (scenes::actual_menu_scenes)
		{
		case menu::scenes::menu_scenes::general:
			
			DrawTextureRec(btn[0].texture, btn[0].sourceRec, { btn[0].bounds.x, btn[0].bounds.y }, WHITE); // Draw button frame
			DrawTextureRec(btn[1].texture, btn[1].sourceRec, { btn[1].bounds.x, btn[1].bounds.y }, WHITE); // Draw button frame
			DrawTextureRec(btn[2].texture, btn[2].sourceRec, { btn[2].bounds.x, btn[2].bounds.y }, WHITE); // Draw button frame
			DrawTextureRec(btn[3].texture, btn[3].sourceRec, { btn[3].bounds.x, btn[3].bounds.y }, WHITE); // Draw button frame
			break;

		case menu::scenes::menu_scenes::intro:
			DrawText(&intro[0], betterColocation::screenMiddleWidth() - MeasureText(&intro[0], betterColocation::screenMiddleHeight() /16)/2, betterColocation::screenMiddleHeight() - betterColocation::screenMiddleHeight() / 16 /2, betterColocation::screenMiddleHeight() / 16, RED);
			DrawText(&intro2[0], betterColocation::screenMiddleWidth() - MeasureText(&intro2[0], betterColocation::screenMiddleHeight() / 20) / 2, betterColocation::screenMiddleHeight() - betterColocation::screenMiddleHeight() / 20 / 2 + 70, betterColocation::screenMiddleHeight() / 20, RED);
			
			break;
		case menu::scenes::menu_scenes::options:
			switch (scenes::ops)
			{
			case menu::scenes::options::one:
				DrawText("Fx",betterColocation::screenMiddleWidth()-MeasureText("Fx", betterColocation::screenHeight100() * 10)/2,betterColocation::screenHeight100()*10, betterColocation::screenHeight100() * 10,GREEN);
				DrawText("Music", betterColocation::screenMiddleWidth() - MeasureText("Music", betterColocation::screenHeight100() * 10)/2, betterColocation::screenHeight100() * 50, betterColocation::screenHeight100() * 10, GREEN);
				menuDraw::easyDrawB(btn[(short)Bts::mas]);
				menuDraw::easyDrawB(btn[(short)Bts::menos]);
				menuDraw::easyDrawB(btn[(short)Bts::barras]);
				menuDraw::easyDrawB(btn[(short)Bts::back]);
				menuDraw::easyDrawB(btn[(short)Bts::next]);
				menuDraw::easyDrawB(btn[(short)Bts::mas2]);
				menuDraw::easyDrawB(btn[(short)Bts::menos2]);
				menuDraw::easyDrawB(btn[(short)Bts::barras2]);
				break;
			case menu::scenes::options::two:
				DrawText("Resolution", betterColocation::screenMiddleWidth() - MeasureText("Resolution", betterColocation::screenHeight100() * 10)/2, betterColocation::screenHeight100() * 10, betterColocation::screenHeight100() * 10, GREEN);

				menuDraw::easyDrawB(btn[(short)Bts::back]);
				menuDraw::easyDrawB(btn[(short)Bts::mas]);
				menuDraw::easyDrawB(btn[(short)Bts::menos]);
				menuDraw::NextRes();
				menuDraw::easyDrawB(btn[12]);
				break;
			default:
				break;
			}
			
			break;
		case menu::scenes::menu_scenes::credits:
			DrawTexture(credits,0,0,WHITE);
			if (arkanoid::game::testing)
			{
				DrawText("Test mod active, move the ball whit j,k,i,l, re launch and stop (y)", betterColocation::screenMiddleWidth()- MeasureText("Test mod active, move the ball whit j,k,i,l, re launch and stop (y)", betterColocation::screenHeight100())/2, betterColocation::screenHeight100() * 90, betterColocation::screenHeight100(),RED);
			}
			
			DrawTextureRec(btn[4].texture, btn[4].sourceRec, { btn[4].bounds.x, btn[4].bounds.y }, WHITE); // Draw button frame
		default:
			break;
		}
		
		


	}
	void deinit()
	{
		for (short i = 0; i < quantity_buttons; i++)
		{
			UnloadTexture(btn[i].texture);
		}
		UnloadTexture(ground);
		UnloadSound(fxButton);
		UnloadMusicStream(music);
		UnloadTexture(credits);
		

	}
}