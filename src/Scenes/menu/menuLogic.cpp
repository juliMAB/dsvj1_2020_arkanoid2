#include "Scenes/menu/menuLogic.h"

namespace menuLogic
{
	

	
	namespace init
	{
		void chargeButton(button a[], short quantity_buttons)
		{
			for (short i = 0; i < quantity_buttons; i++)
			{
				a[i].texture.height *= GetScreenHeight() / 160;								    //cambio altura de textura.
				a[i].texture.width *=  GetScreenWidth () / 120;								    //cambio ancho de textura.
				a[i].frameHeight = a[i].texture.height / 3;								//cambio la altura de los frames dependiendo de cuantos son.
				a[i].sourceRec = { 0, 0, (float)a[i].texture.width, (float)a[i].frameHeight };   //imagen interna. En marco.
				a[i].bounds =																//imagen externa. El marco.
				{
					0,																	//x.
					0,																	//y.
					(float)a[i].texture.width, (float)a[i].frameHeight						// size.
				};
				a[i].touch = false;														//si esta siendo tocado.
			}
			

			//externamente debo omodificar solo la pocicion en x,y.
		}
		void chargeButtonBack(button & a)
		{
			a.texture.height /= 3;						 //cambio altura de textura.
			a.texture.width /= 3;						 //cambio ancho de textura.
			a.frameHeight /= 3;
			a.sourceRec = { 0, 0, (float)a.texture.width, (float)a.frameHeight };
			a.bounds =
			{
				0,
				(float)GetScreenHeight() - a.texture.height / 2,
				(float)a.texture.width,
				(float)a.frameHeight
			};

		}
		void chargeButtonNext(button& a)
		{
			a.texture.height /= 3;						 //cambio altura de textura.
			a.texture.width /= 3;						 //cambio ancho de textura.
			a.frameHeight /= 3;
			a.sourceRec = { 0, 0, (float)a.texture.width, (float)a.frameHeight };
			a.bounds =
			{
				(float)GetScreenWidth() - a.texture.width,
				(float)GetScreenHeight() - a.texture.height / 2,
				(float)a.texture.width,
				(float)a.frameHeight
			};
		}
		void chargeButtonMas(button & a)
		{
			a.bounds =
			{
				(float)betterColocation::screenWidth100() * 80 - (float)a.texture.width,
				(float)betterColocation::screenHeight100() * 40 - (float)a.texture.width,
				(float)a.texture.width,
				(float)a.frameHeight * 2
				
			};
		}
		void chargeButtonMenos(button & a)
		{
			a.bounds =
			{
				(float)betterColocation::screenWidth100() * 25 - (float)a.texture.width,
				(float)betterColocation::screenHeight100() * 40 - (float)a.texture.width,
				(float)a.texture.width,
				(float)a.frameHeight
			};
		}
		void chargeButtonBarrs(button & a)
		{
			a.texture.height *= 2;
			a.texture.width  *= 2;
			a.frameHeight = a.texture.height / 6;
			a.sourceRec = { 0, 0, (float)a.texture.width, (float)a.frameHeight };
			a.bounds = 
			{
				(float)betterColocation::screenWidth100 () * 60 - a.texture.width,
				(float)betterColocation::screenHeight100() * 40 - a.frameHeight,
				(float)a.texture.width,
				(float)a.frameHeight
			};
		}
		void chargeButtonPos(button btn[])
		{
			
				/*PLAY*/		btn[0].bounds = { (float)(betterColocation::screenMiddleWidth() - btn[0].texture.width / 2), (float)(betterColocation::screenMiddleHeight() - btn[0].frameHeight * 4),(float)btn[0].texture.width,(float)btn[0].frameHeight };
				/*OPTI*/		btn[2].bounds = { (float)(betterColocation::screenMiddleWidth() - btn[2].texture.width / 2), (float)(betterColocation::screenMiddleHeight() - btn[2].frameHeight * 2),(float)btn[0].texture.width,(float)btn[0].frameHeight };
				/*CRED*/		btn[3].bounds = { (float)(betterColocation::screenMiddleWidth() - btn[3].texture.width / 2), (float)(betterColocation::screenMiddleHeight() + btn[3].frameHeight * 0),(float)btn[0].texture.width,(float)btn[0].frameHeight };
				/*EXIT*/		btn[1].bounds = { (float)(betterColocation::screenMiddleWidth() - btn[1].texture.width / 2), (float)(betterColocation::screenMiddleHeight() + btn[1].frameHeight * 2),(float)btn[0].texture.width,(float)btn[0].frameHeight };

			
		
		}
		void chargeButtonMas2(button& a)
		{
			a.bounds =
			{
				(float)betterColocation::screenWidth100() * 80 - (float)a.texture.width,
				(float)betterColocation::screenHeight100() * 80 - (float)a.texture.width,
				(float)a.texture.width,
				(float)a.frameHeight * 2
			};
		}
		void chargeButtonMenos2(button& a)
		{
			a.bounds =
			{
				
				(float)betterColocation::screenWidth100() * 25 - (float)a.texture.width,
				(float)betterColocation::screenHeight100() * 80 - (float)a.texture.width,
				(float)a.texture.width,
				(float)a.frameHeight
			};
		}
		void chargeButtonBarrs2(button& a)
		{
			a.texture.height *= 2;
			a.texture.width *= 2;
			a.frameHeight = a.texture.height / 6;
			a.sourceRec = { 0, 0, (float)a.texture.width, (float)a.frameHeight };
			a.bounds =
			{
				(float)betterColocation::screenWidth100() * 60 - a.texture.width,
				(float)betterColocation::screenHeight100() * 80 - a.frameHeight,
				(float)a.texture.width,
				(float)a.frameHeight
			};
		}
		void chargeButtonAccept(button& a)
		{
			a.texture.height /= 2;
			a.texture.width /= 2;
			a.frameHeight = a.texture.height / 3;
			a.sourceRec = { 0, 0, (float)a.texture.width, (float)a.frameHeight };
			a.bounds =
			{
				(float)betterColocation::screenWidth100() * 50 - a.texture.width/2,
				(float)betterColocation::screenHeight100() * 40 - a.frameHeight/2,
				(float)a.texture.width,
				(float)a.frameHeight
			};
		}
		void chargeTextures(button btn[])
		{
			btn[0].texture = LoadTexture("resources/play.png");
			btn[1].texture = LoadTexture("resources/exit.png");
			btn[2].texture = LoadTexture("resources/options.png");
			btn[3].texture = LoadTexture("resources/credits.png");
			btn[4].texture = LoadTexture("resources/back.png");
			btn[5].texture = LoadTexture("resources/next.png");
			btn[6].texture = LoadTexture("resources/mas.png");
			btn[7].texture = LoadTexture("resources/menos.png");
			btn[8].texture = LoadTexture("resources/barras6states.png");
			btn[9].texture = LoadTexture("resources/mas.png");
			btn[10].texture = LoadTexture("resources/menos.png");
			btn[11].texture = LoadTexture("resources/barras6states.png");
			btn[12].texture = LoadTexture("resources/accept.png");
			menu::ground = LoadTexture("resources/ground.png");
			menu::ground.height = GetScreenHeight();
			menu::ground.width = GetScreenWidth();
			menu::credits = LoadTexture("resources/Creditos.png");
			menu::credits.height = GetScreenHeight();
			menu::credits.width = GetScreenWidth();
		}
		void chargeAudios()
		{
			menu::fxButton = LoadSound("resources/pop.wav");
			menu::music =	 LoadMusicStream("resources/pixelsong.mp3");
		}
		void chargeMusic()
		{
			SetSoundVolume(menu::fxButton, arkanoid::game::fxVolume); //bajarle el bolumen a los tocs(botones).
			SetMusicVolume(menu::music, arkanoid::game::musicVolume); //bajarle el bolumen a las musica.
			PlayMusicStream(menu::music);
		}
		void allBUTTONS(short quantity_buttons, button btn[])
		{
			chargeTextures	 (btn   );
			chargeButton(btn, quantity_buttons);
			chargeButtonPos	 (btn   );
			chargeButtonBack (btn[4]);
			chargeButtonNext (btn[5]);
			chargeButtonMas  (btn[6]);
			chargeButtonMenos(btn[7]);
			chargeButtonBarrs(btn[8]);
			chargeButtonMas2  (btn[9]);
			chargeButtonMenos2(btn[10]);
			chargeButtonBarrs2(btn[11]);
			chargeButtonAccept(btn[12]);
		}
	}
	namespace update
	{
		//boton general.
		void updatePlay(button& a)
		{
			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					StopMusicStream(menu::music);
					arkanoid::game::currentScene = arkanoid::game::Scene::Game;
				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
		}
		void updateOp  (button& a)
		{
			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					menu::scenes::actual_menu_scenes = menu::scenes::menu_scenes::options;
					menu::scenes::ops = menu::scenes::options::one;
				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
		}
		void updateCred(button& a)
		{
			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					menu::scenes::actual_menu_scenes = menu::scenes::menu_scenes::credits;
				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
		}
		void updateExit(button& a)
		{
			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					StopMusicStream(menu::music);

					arkanoid::game::gameOver = true;
				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
		}

		//-------------------------------------
		void updateGeneral(button a[])
		{
			updatePlay(a[0]);
			updateOp  (a[2]);
			updateCred(a[3]);
			updateExit(a[1]);
		}
		//-------------------------------------
		//opts.
		bool updateBack(button & a)
		{
			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					return true;
					
				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
			return false;
		}
		bool updateNext(button & a)
		{
			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					return true;
				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
			return false;
		}

		bool updateMore(button & a)
		{

			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					
					
						return true;
						
					
				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
			return false;
		}
		bool updateLess(button & a)

		{

			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					
						return true;
						
					
				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
			return false;
		}
		void updateBarrs(button& a, float s)
		{
			if (s >= 0)
			{
				a.state = 0;
				if (s > 0.1f)
				{
					a.state = 1;
					if (s > 0.4f)
					{
						a.state = 2;
						if (s > 0.6f)
						{
							a.state = 3;
							if (s > 0.8f)
							{
								a.state = 4;
								if (s > 0.9f)
								{
									a.state = 5;
								}
							}
						}
					}
				}

			}
			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
		}
		bool updateAccept(button& a)
		{

			if (CheckCollisionPointRec(menu::mousePoint, a.bounds)) //si estoy por sobre el boton.
			{
				if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) a.state = 2;
				else a.state = 1;
				if (!a.touch)
				{
					PlaySound(menu::fxButton);
					a.touch = true;
				}


				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{


					return true;


				}
			}
			else
			{
				a.touch = false;
				a.state = 0;
			}

			a.sourceRec.y = (float)a.state * (float)a.frameHeight;
			return false;
		}

		//-------------------------------------
		void updateOptions(button a[])
		{
			if(updateBack(a[4])) 
			{
				menu::scenes::actual_menu_scenes = menu::scenes::menu_scenes::general;
			}
			if (updateNext(a[5]))
			{
				menu::scenes::ops = menu::scenes::options::two;
			}
			if (updateMore(a[6]))
			{
				if (arkanoid::game::fxVolume <= 1)
				{
					arkanoid::game::fxVolume += 0.1f;
					SetSoundVolume(menu::fxButton, arkanoid::game::fxVolume);
				}
			}
			if (updateLess(a[7]))
			{
				if (arkanoid::game::fxVolume > 0.0001f)
				{
					arkanoid::game::fxVolume -= 0.1f;
					SetSoundVolume(menu::fxButton, arkanoid::game::fxVolume);
				}
				
			}
			updateBarrs(a[8],arkanoid::game::fxVolume);
			if (updateMore(a[9]))
			{
				if (arkanoid::game::musicVolume <= 1) 
				{
					arkanoid::game::musicVolume += 0.1f;
					SetMusicVolume(menu::music, arkanoid::game::musicVolume);
				}
				
			}
			if (updateLess(a[10]))
			{
				if (arkanoid::game::musicVolume > 0.0001f)
				{
					arkanoid::game::musicVolume -= 0.1f;
					SetMusicVolume(menu::music, arkanoid::game::musicVolume);
				}
			}
			updateBarrs(a[11], arkanoid::game::musicVolume);

		}
		//-------------------------------------
		void updateOptions2(button a[])
		{
			if (updateBack(a[4]))
			{
				menu::scenes::actual_menu_scenes = menu::scenes::menu_scenes::general;
			}
			if (updateMore(a[6]))
			{
				if (arkanoid::game::NroResolution<4)
				{
					arkanoid::game::NroResolution++;
				}
			}
			if (updateLess(a[7]))
			{
				if (arkanoid::game::NroResolution > 0)
				{
					arkanoid::game::NroResolution--;
				}
			}
			
			if (updateAccept(a[12]))
			{
				
				SetWindowSize((int)resolution::possible_resolution[arkanoid::game::NroResolution].x, (int)resolution::possible_resolution[arkanoid::game::NroResolution].y);
			//se tiene que volver a iniciar todo.
				arkanoid::game::changeResolution = true;
			
			}
		}
	}
	
}