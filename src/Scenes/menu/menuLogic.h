#ifndef MENULOGIC_H
#define MENULOGIC_H
#include "raylib.h"
#include "ExternV/entities.h"
#include "ExternV/general_variables.h"
#include "Scenes/both/posFuns.h"
#include "controler/game.h"
namespace menuLogic
{
	//bool touchBoton(Rectangle,Vector2);

	//bool touchBotonActived(Rectangle, Vector2,bool);
	namespace init
	{
		void allBUTTONS(short quantity_buttons, button btn[]);

		void chargeAudios();

		void chargeMusic();
	}
	namespace update
	{
		void updateGeneral(button a[]);
		void updateOptions(button a[]);
		void updateOptions2(button a[]);
	}
}


#endif