#include "draw.h"


namespace draw
{
	//scenes::Scene currentScene;

	void showFps()
	{
		DrawFPS(0, 0);
	}

	void showCurrentScene()
	{
		using namespace arkanoid::game;
		switch (currentScene)
		{
		case arkanoid::game::Scene::Menu:
			DrawText(&mm[0], GetScreenWidth() - MeasureText(&mm[0], 10), 0, 10, RED);
			break;
		case arkanoid::game::Scene::Game:
			DrawText(&mg[0], GetScreenWidth() - MeasureText(&mg[0], 10), 0, 10, RED);
			break;
		default:
			break;
		}
	}
	void showResolution()
	{
		int x = GetScreenWidth();
		int y = GetScreenHeight();
		DrawText(FormatText("%ix%i",x, y), 0, GetScreenHeight() - 10, 10, RED);
	}
}
/*case scenes::x::M: DrawText(&mm[0], screen::Width - MeasureText(&mm[0], 10), 0, 10, RED);
			break;
		case Scene::Game:DrawText(&mg[0], screen::Width - MeasureText(&mg[0], 10), 0, 10, RED);
			break;*/