#include "Scenes/both/posFuns.h"

namespace betterColocation
{
	int screenMiddleHeight()
	{
		return  GetScreenHeight()/2;
	}
	int screenMiddleWidth()
	{
		return  GetScreenWidth() / 2;
	}
	int screenHeight100()
	{
		return GetScreenHeight() / 100;
	}
	int screenWidth100()
	{
		return GetScreenWidth() / 100;
	}
}