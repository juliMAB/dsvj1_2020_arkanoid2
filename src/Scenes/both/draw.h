#ifndef DRAW_H
#define DRAW_H

#include "ExternV/general_variables.h"
#include "raylib.h"
#include "ExternV/Texts.h"
#include "controler/game.h"

namespace draw
{
	//scenes::Scene currentScene;

	void showFps();

	void showCurrentScene();

	void showResolution();
}


#endif