#include "raylib.h"
#ifndef POSFUNS_H
#define POSFUNS_H
namespace betterColocation
{
	int screenMiddleHeight();

	int screenMiddleWidth();

	int screenHeight100();

	int screenWidth100();
}
#endif