#include "gameplay.h"
#define FPSGROUND 45;
#define FPSPLAYER 15;
#define TIMERBLOCKS 75;

namespace gameplay
{
	short level=0;
	short score=0;
	int totalscore = 0;
    namespace animatedTimer
    {
        short player;        //player time change de anim.
        short ground;        //ground time change de anim.
    
    }
    
    
    
	namespace entities
	{
		Ball ball = {0};
		Player player = {0};
		Brick bricks[LINES_OF_BRICKS][BRICKS_PER_LINE] = { 0 };
	
       
        namespace ground
        {
            Texture2D texture;
            int frameHightGround;
            Rectangle groundSource;
            Rectangle groundBounds;
            short groundState;
			Sound sfall;
			Sound swalls;
			Music sground;

        }
        namespace uses_bricks
        {
            int frameHightBlock;
            Sound bricks_crack;
            Texture2D bricks_texture;
            Vector2 brickSize;
        }
	}
	namespace logic
	{
		bool gameOver = false;
		bool pause = false;
        bool mouse = false;
		bool win = false;
	}

	extern void init()
	{
       GameplayLogic::init::chargeAll();
	   gameplay::score = 0;
	}
	extern void update()
	{
		UpdateMusicStream(entities::ground::sground);
		if (!IsMusicPlaying(entities::ground::sground))
		{
			PlayMusicStream(entities::ground::sground);
		}
        GameplayLogic::update::all();
    }
	extern void draw() 
	{

        using namespace entities;

        DrawTextureRec(ground::texture, ground::groundSource, {0, ground::groundBounds.y}, GRAY);
		if (!logic::gameOver)
		{
			//draw Player
            DrawTextureRec(player.texture,player.sourceRec, { player.bounds.x-player.texture.width/2, player.bounds.y}, WHITE);
           
			// Draw player lives
			for (int i = 0; i < player.life; i++)  
			DrawRectangle(20 + 40 * i, GetScreenHeight() - GetScreenHeight()/30, 35, 10, LIGHTGRAY);

			// Draw ball
			DrawTextureRec(ball.texture, { 0,0,(float)ball.radius*2,(float)ball.radius*2 }, { ball.position.x - ball.radius,ball.position.y - ball.radius}, ball.color);
			
			// Draw bricks
			for (int i = 0; i < LINES_OF_BRICKS; i++)
			{
				for (int j = 0; j < BRICKS_PER_LINE; j++)
				{
					if (bricks[i][j].active)
					{
						if ((i + j) % 2 == 0) DrawTextureRec(uses_bricks::bricks_texture, { 0,(float)bricks[i][j].state * uses_bricks::frameHightBlock , uses_bricks::brickSize.x,uses_bricks::brickSize.y }, { bricks[i][j].position.x - uses_bricks::brickSize.x / 2 , bricks[i][j].position.y - uses_bricks::brickSize.y / 2 }, bricks[i][j].color);
						else DrawTextureRec(uses_bricks::bricks_texture, { 0,(float)bricks[i][j].state * uses_bricks::frameHightBlock , uses_bricks::brickSize.x,uses_bricks::brickSize.y }, { bricks[i][j].position.x- uses_bricks::brickSize.x/2 , bricks[i][j].position.y- uses_bricks::brickSize.y/2 }, bricks[i][j].color);
					}
					else
					{
						if (bricks[i][j].self_Destruction>0)
						{
							DrawTextureRec(uses_bricks::bricks_texture, { 0,(float)bricks[i][j].state * uses_bricks::frameHightBlock , uses_bricks::brickSize.x,uses_bricks::brickSize.y }, { bricks[i][j].position.x - uses_bricks::brickSize.x / 2 , bricks[i][j].position.y - uses_bricks::brickSize.y / 2 }, bricks[i][j].color);

						}
					}
				}
			}

			// Draw Puntos
			DrawText(FormatText("SCORE: %i", gameplay::score), betterColocation::screenMiddleWidth()- MeasureText("SCORE: %i", GetScreenHeight() / 20)/2, GetScreenHeight() - 10, 10, RED);

			if (logic::pause) DrawText("GAME PAUSED", betterColocation::screenMiddleWidth() - MeasureText("GAME PAUSED", GetScreenHeight()/20) / 2, betterColocation::screenMiddleHeight() - GetScreenHeight() / 20, GetScreenHeight() / 20, GRAY);
			if (logic::pause) DrawText("press G to back", betterColocation::screenMiddleWidth() - MeasureText("press G to back", GetScreenHeight() / 20) / 2, betterColocation::screenMiddleHeight() - GetScreenHeight() / 160, GetScreenHeight() / 20, GRAY);

		}
		else
		{
			if (gameplay::level!=5)
			{
				DrawText("PRESS [ENTER] TO NEXT LVL", GetScreenWidth() / 2 - MeasureText("PRESS [ENTER] TO NEXT LVL", GetScreenHeight() / 40) / 2, GetScreenHeight() / 2 - GetScreenHeight() / 16, GetScreenHeight() / 40, GRAY);

			}
			else
			{
				DrawText("CONGRATS, your Win", GetScreenWidth() / 2 - MeasureText("CONGRATS, your Win", GetScreenHeight()/40) / 2,  betterColocation::screenHeight100()*80, GetScreenHeight()/40, GRAY);
				DrawText(FormatText("SCORE: %i", gameplay::totalscore), GetScreenWidth() / 2 - MeasureText("SCORE: 000", GetScreenHeight() / 40) / 2, betterColocation::screenHeight100() * 70, GetScreenHeight() / 40, GRAY);
				DrawText("PRESS [ENTER] TO PLAY AGAIN", GetScreenWidth() / 2 - MeasureText("PRESS [ENTER] TO PLAY AGAIN", GetScreenHeight() / 40) / 2, betterColocation::screenHeight100() * 60, GetScreenHeight() / 40, GRAY);
				DrawText("press G to back", betterColocation::screenMiddleWidth() - MeasureText("press G to back", GetScreenHeight() / 20) / 2, betterColocation::screenHeight100() * 50, GetScreenHeight() / 20, GRAY);
			}
		
		}
#if TESTING
		//player
		DrawRectangleLines(player.bounds.x - player.texture.width / 2, player.bounds.y, player.texture.width, player.frameHeight, GREEN);
		//bloques
		for (int i = 0; i < LINES_OF_BRICKS; i++)
		{
			for (int j = 0; j < BRICKS_PER_LINE; j++)
			{
				if (bricks[i][j].active)
				{
					if ((i + j) % 2 == 0) DrawRectangleLines(  bricks[i][j].position.x - uses_bricks::brickSize.x / 2 , bricks[i][j].position.y - uses_bricks::brickSize.y / 2 , uses_bricks::brickSize.x, uses_bricks::brickSize.y, GREEN);
					else DrawRectangleLines(bricks[i][j].position.x - uses_bricks::brickSize.x / 2, bricks[i][j].position.y - uses_bricks::brickSize.y / 2, uses_bricks::brickSize.x, uses_bricks::brickSize.y, GREEN);
				}
				else
				{	
					DrawRectangleLines(bricks[i][j].position.x - uses_bricks::brickSize.x / 2, bricks[i][j].position.y - uses_bricks::brickSize.y / 2, uses_bricks::brickSize.x, uses_bricks::brickSize.y, RED);
				}
			}
		}
		//ball
		DrawTextureRec(ball.texture, { 0,0,(float)ball.radius * 2,(float)ball.radius * 2 }, { ball.position.x - ball.radius,ball.position.y - ball.radius }, ball.color);
		DrawRectangleLines(ball.position.x - ball.texture.width / 2, ball.position.y - ball.texture.height / 2, ball.texture.width, ball.texture.height, GREEN);



#endif
		
	}
	extern void deinit() 
	{
        UnloadTexture(entities::player.texture);
        UnloadSound(entities::uses_bricks::bricks_crack);
		UnloadTexture(entities::ground::texture);
		UnloadTexture(entities::uses_bricks::bricks_texture);
		UnloadSound(entities::ground::sfall);
		UnloadSound(entities::ground::swalls);
		UnloadMusicStream(entities::ground::sground );

		
	}
}