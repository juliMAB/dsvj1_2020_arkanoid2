#ifndef MENU_H
#define MENU_H
#include "raylib.h"
#include "ExternV/Scenes.h"
#include "ExternV/general_variables.h"
#include "controler/game.h"
#include "Scenes/menu/menuDraw.h"
#include "Scenes/menu/menuLogic.h"
#include "ExternV/entities.h"


namespace menu
{
	extern Music music;
	extern Sound fxButton;
	extern Vector2 mousePoint;
	extern Texture2D ground;
	extern Texture2D credits;
	namespace scenes
	{
		enum class menu_scenes
		{
			general,
			intro,
			options,
			credits
		};
		extern menu_scenes actual_menu_scenes;
		enum class options { one, two };
		extern options ops;
	}
	//Rectangle A;
	extern void init();
	extern void update();
	extern void draw();
	extern void deinit();
}
#endif