
#include "gameplayLogic.h"
#define MAX_GROUNDFRAME 2
#define MAX_BLOCKFRAME 3
#define TIMERBLOCKS 75
namespace GameplayLogic
{
    void backgroundAnimation(short& actualframe, short cantframes)
    {

        static bool up = true;

        //static short frame = 0;

        if (up)
        {
            actualframe++;
        }
        else
        {
            actualframe--;
        }



        if (actualframe >= cantframes)
        {
            up = false;
        }
        else if (actualframe <= 0)
        {
            up = true;
        }

    }

    namespace init
    {
        using namespace gameplay;
        void chargeAnims()
        {
            //animations
            using namespace gameplay;
            entities::ground::groundState = 0;
            animatedTimer::player = FPSPLAYER;
            animatedTimer::ground = FPSGROUND;
        }
        void chargeGround()
        {
            entities::ground::texture = LoadTexture("resources/ArkanoidFondo.png");
            entities::ground::texture.height = GetScreenHeight() * 3;
            entities::ground::texture.width = GetScreenWidth();
            entities::ground::frameHightGround = entities::ground::texture.height / 3;
            entities::ground::groundSource = { 0, 0, (float)entities::ground::texture.width, (float)entities::ground::frameHightGround };
            entities::ground::sfall = LoadSound("resources/caida.wav");
            entities::ground::swalls = LoadSound("resources/pomio.wav");
            entities::ground::sground = LoadMusicStream("resources/musicgame.mp3");
        }
        void chargeBricks()
        {
            entities::uses_bricks::brickSize = { (float)GetScreenWidth() / (float)BRICKS_PER_LINE, (float)GetScreenHeight()/20 };
            entities::uses_bricks::bricks_texture = LoadTexture("resources/blockX.png");
            entities::uses_bricks::bricks_texture.height = static_cast<int>(entities::uses_bricks::brickSize.y * 7);
            entities::uses_bricks::bricks_texture.width = static_cast<int>(entities::uses_bricks::brickSize.x);
            entities::uses_bricks::frameHightBlock = entities::uses_bricks::bricks_texture.height / 7;
            entities::uses_bricks::bricks_crack = LoadSound("resources/crack.wav");
            SetSoundVolume(entities::uses_bricks::bricks_crack, arkanoid::game::fxVolume);
            int initialDownPosition = GetScreenHeight() / 10;
            Color opColors[6] = { GRAY,RED,YELLOW,BLUE,PINK,GREEN };
            for (int i = 0; i < LINES_OF_BRICKS; i++)
            {
                for (int j = 0; j < BRICKS_PER_LINE; j++)
                {
                    entities::bricks[i][j].position = { j * entities::uses_bricks::brickSize.x + entities::uses_bricks::brickSize.x / 2, i * entities::uses_bricks::brickSize.y + initialDownPosition };
                    entities::bricks[i][j].active = true;
                    entities::bricks[i][j].color = opColors[i];
                    entities::bricks[i][j].self_Destruction = TIMERBLOCKS;
                    entities::bricks[i][j].state = 0;
                    entities::bricks[i][j].lifes = 1;

                }
            }
        }
        void chargeMap()
        {
            for (int i = 0; i < LINES_OF_BRICKS; i++)
            {
                for (int j = 0; j < BRICKS_PER_LINE; j++)
                {
                    entities::bricks[i][j].active = lvl::nivel[gameplay::level][i][j];
                    if (!entities::bricks[i][j].active)
                    {
                        entities::bricks[i][j].self_Destruction = 0;
                    }
                }
            }
        }
        void giveLiveBlocks() 

        {
            if (gameplay::level < 5)
            {
                for (int i = 0; i < LINES_OF_BRICKS; i++)
                {
                    for (int j = 0; j < BRICKS_PER_LINE; j++)
                    {
                        
                         entities::bricks[i][j].active = lvl::nivel[gameplay::level][i][j];
                        

                        if (entities::bricks[i][j].active)
                        {
                            switch (gameplay::level) //depende del nivel la vida que voy a dar a los bloques.
                            {
                            case 0: entities::bricks[i][j].lifes = 1; //todos sus bloques se inician en 1.
                                break;
                            case 1:
                                if (i == 5 || i == 3 || i == 1)
                                {
                                    entities::bricks[i][j].lifes = 2;
                                }
                                else
                                {
                                    entities::bricks[i][j].lifes = 1;
                                }
                                break;
                            case 2:
                                entities::bricks[i][j].lifes = 2;
                                break;
                            case 3:
                                if (i == 4)
                                {
                                    entities::bricks[i][j].lifes = 3;
                                }
                                else if (i == 3)
                                {
                                    entities::bricks[i][j].lifes = 2;
                                }
                                else
                                {
                                    entities::bricks[i][j].lifes = 1;
                                }
                                break;
                            case 4:
                                if (i == 5)
                                {
                                    entities::bricks[i][j].lifes = 4;
                                }
                                else if (i == 4)
                                {
                                    entities::bricks[i][j].lifes = 3;
                                }
                                else if (i == 3)
                                {
                                    entities::bricks[i][j].lifes = 2;
                                }
                                else
                                {
                                    entities::bricks[i][j].lifes = 1;
                                }
                                break;
                            case 5:
                                if (i == 5)
                                {
                                    entities::bricks[i][j].lifes = 1;
                                }
                                else if (i == 4)
                                {
                                    entities::bricks[i][j].lifes = 1;
                                }
                                else if (i == 3)
                                {
                                    entities::bricks[i][j].lifes = 2;
                                }
                                else if (i == 2)
                                {
                                    entities::bricks[i][j].lifes = 3;
                                }
                                else if (i == 1)
                                {
                                    entities::bricks[i][j].lifes = 3;
                                }
                                else if (i == 0)
                                {
                                    entities::bricks[i][j].lifes = 4;
                                }

                                break;
                            }
                        }
                    }
                }
            }
            else // te pasaste todos los lvls.
            {
                gameplay::logic::pause = true;
            }
        }
        void chargePlayer()
        {
            entities::player.life = PLAYER_MAX_LIFE;
            entities::player.texture = LoadTexture("resources/pad.png");
            entities::player.texture.height *= ((int)GetScreenHeight() / 266);
            entities::player.texture.width  *= ((int)GetScreenWidth () / 200);
            entities::player.frameHeight = entities::player.texture.height / 5;
            entities::player.sourceRec = { 0, 0, (float)entities::player.texture.width, (float)entities::player.frameHeight };
            entities::player.bounds = {
                (float)(betterColocation::screenMiddleWidth()) ,
                (float)betterColocation::screenHeight100() * 90,
                (float)entities::player.texture.width,
                (float)entities::player.frameHeight };

        }
        void chargeBall()
        {
            entities::ball.texture = LoadTexture("resources/ball.png");
            entities::ball.texture.height = ((int)GetScreenHeight() / 35);
            entities::ball.texture.width = ((int)GetScreenWidth() / 30);
            entities::ball.color = RED;
            entities::ball.position = 
            { 
                (float)betterColocation::screenMiddleWidth(),
                (float)GetScreenHeight() * 7 / 8 - GetScreenHeight() / 26
            };
            entities::ball.speed = { 0, 0 };
            entities::ball.radius = static_cast<float>(GetScreenWidth()/60);
            entities::ball.active = false;
        }
        void chargeAll()
        {
           chargeAnims();
           chargeGround();
           chargeBricks();
           chargeMap();
           giveLiveBlocks();
           chargePlayer();
           chargeBall();  
        } 
        //-----------------------
    }

    namespace update
    {
        using namespace gameplay;
        using namespace entities;
        void pressPause()
        {
            if (IsKeyPressed('P')) logic::pause = !logic::pause;
        }
        void playerMove()
        {
            // Player movement logic

            if (IsKeyDown(KEY_LEFT)|| IsKeyDown(KEY_A))
            {
                player.bounds.x -= GetScreenWidth()/120;

                if (player.state != 4 && animatedTimer::player < 0)
                {
                    player.state = 4;
                    animatedTimer::player = FPSPLAYER;
                }
                else if (player.state != 3 && animatedTimer::player < 0)
                {
                    player.state = 3;
                    animatedTimer::player = FPSPLAYER;
                }

                if ((player.bounds.x - player.bounds.width / 2) <= 0)
                {
                    player.bounds.x = player.bounds.width / 2;

                }
            }
            else if (IsKeyDown(KEY_RIGHT)|| IsKeyDown(KEY_D))
            {
                player.bounds.x += GetScreenWidth() / 120;
                if (player.state != 1 && animatedTimer::player < 0)
                {
                    player.state = 1;
                    animatedTimer::player = FPSPLAYER;
                }
                else if (player.state != 2 && animatedTimer::player < 0)
                {
                    player.state = 2;
                    animatedTimer::player = FPSPLAYER;
                }

                if ((player.bounds.x + player.bounds.width / 2) >= GetScreenWidth())
                {
                    player.bounds.x = GetScreenWidth() - player.bounds.width / 2;
                }
            }
            else
            {
                player.state = 0;
            }
        }
        void hacks()
        {
            if (IsKeyDown(KEY_C))
            {
                for (int i = 0; i < LINES_OF_BRICKS; i++)
                {
                    for (int j = 0; j < BRICKS_PER_LINE; j++)
                    {
                        entities::bricks[i][j].active = false;
                    }
                }
            }
            //move
            if (IsKeyDown(KEY_I))
            {
                ball.position.y -= 5;
            }
            if (IsKeyDown(KEY_L))
            {
                ball.position.x += 5;
            }
            if (IsKeyDown(KEY_K))
            {
                ball.position.y += 5;
            }
            if (IsKeyDown(KEY_J))
            {
                ball.position.x -= 5;
            }
            //frezze
            if (IsKeyReleased(KEY_Y))
            {
                if (ball.speed.y != 0)
                {
                   
                    ball.speed.y = 0; //solo funciona el else, por alguna extra�a razon...
                }
                else
                {
                    ball.speed = { 0, -(float)GetScreenHeight() / 160 };
                }
                
            }

            //reiniciar.
            if (IsKeyDown(KEY_R))
            {
                init::chargeAll();
            }

        }
        void ballLauncher()
        {
            if (!ball.active)
            {
                if (IsKeyPressed(KEY_SPACE))
                {
                    ball.active = true;
                    ball.speed = { 0, - (float)GetScreenHeight()/160 };
                }
            }
        }
        void BallMove()
        {
            if (ball.active)
            {
                ball.position.x += ball.speed.x;
                ball.position.y += ball.speed.y;
            }
            else
            {
                ball.position = { (float)player.bounds.x, (float)GetScreenHeight() * 7 / 8 - 30 };
            }
        }
        void ball_wall()
        {
            if (((ball.position.x + ball.radius) >= GetScreenWidth()) || ((ball.position.x - ball.radius) <= 0))
            {
                ball.speed.x *= -1;
                PlaySound(gameplay::entities::ground::swalls);
            }
            if ((ball.position.y - ball.radius) <= 0) {
                ball.speed.y *= -1;
            PlaySound(gameplay::entities::ground::swalls); 
            }
            if ((ball.position.y + ball.radius) >= GetScreenHeight())
            {
                PlaySound(gameplay::entities::ground::sfall);
                ball.speed = { 0, 0 };
                ball.active = false;

                player.life--;
            }
        }
        void ball_player() 
        {
            if (CheckCollisionCircleRec({ (ball.position.x),(ball.position.y) }, static_cast<float>(ball.radius),
                {
               player.bounds.x - player.bounds.width / 2, player.bounds.y , player.bounds.width, player.bounds.height
                }))
            {
                if (ball.speed.y > 0)
                {
                    ball.speed.y *= -1;
                    ball.speed.x = (ball.position.x - player.bounds.x) / (player.bounds.width / 2) * 5;
                }
            }
        }
        void ball_bricks()
        {
            for (int i = 0; i < LINES_OF_BRICKS; i++)
            {
                for (int j = 0; j < BRICKS_PER_LINE; j++)
                {
                    if (bricks[i][j].active)
                    {
                        // Hit below
                        if (CheckCollisionCircleRec(ball.position, ball.radius,
                            { bricks[i][j].position.x - uses_bricks::brickSize.x / 2,
                            bricks[i][j].position.y + uses_bricks::brickSize.y / 2 ,
                            uses_bricks::brickSize.x,0 }))
                        {
                            // Hit left
                            if (CheckCollisionCircleRec(ball.position, ball.radius,
                                { bricks[i][j].position.x - uses_bricks::brickSize.x / 2,
                                bricks[i][j].position.y - uses_bricks::brickSize.y / 2 ,
                                0,uses_bricks::brickSize.y }))
                            {

                                PlaySound(entities::uses_bricks::bricks_crack);
                                ball.speed.x *= -1;
                                ball.speed.y *= -1;
                                bricks[i][j].lifes--;
                                if (bricks[i][j].lifes == 0)
                                {
                                    bricks[i][j].active = false;
                                }
                                else
                                {
                                    switch (bricks[i][j].lifes)
                                    {
                                    case 1:bricks[i][j].state = 3;
                                        break;
                                    case 2:bricks[i][j].state = 2;
                                        break;
                                    case 3:bricks[i][j].state = 1;
                                    }
                                }
                            }
                            // Hit right
                            else if (CheckCollisionCircleRec(ball.position, ball.radius,
                                { bricks[i][j].position.x + uses_bricks::brickSize.x / 2,
                                bricks[i][j].position.y - uses_bricks::brickSize.y / 2 ,
                                0,uses_bricks::brickSize.y }))
                            {

                                PlaySound(entities::uses_bricks::bricks_crack);
                                ball.speed.x *= -1;
                                ball.speed.y *= -1;
                                bricks[i][j].lifes--;
                                if (bricks[i][j].lifes == 0)
                                {
                                    bricks[i][j].active = false;
                                }
                                else
                                {
                                    switch (bricks[i][j].lifes)
                                    {
                                    case 1:bricks[i][j].state = 3;
                                        break;
                                    case 2:bricks[i][j].state = 2;
                                        break;
                                    case 3:bricks[i][j].state = 1;
                                    }
                                }
                            }
                            else
                            {
                                PlaySound(entities::uses_bricks::bricks_crack);
                                ball.speed.y *= -1;
                                bricks[i][j].lifes--;
                                if (bricks[i][j].lifes == 0)
                                {
                                    bricks[i][j].active = false;
                                    score += 20;
                                }
                                else
                                {
                                    switch (bricks[i][j].lifes)
                                    {
                                    case 1:bricks[i][j].state = 3;
                                        break;
                                    case 2:bricks[i][j].state = 2;
                                        break;
                                    case 3:bricks[i][j].state = 1;
                                    }
                                }
                            }
                            
                        }
                        // Hit above
                        else if (CheckCollisionCircleRec(ball.position, ball.radius,
                            { bricks[i][j].position.x - uses_bricks::brickSize.x / 2,
                            bricks[i][j].position.y - uses_bricks::brickSize.y / 2 ,
                            uses_bricks::brickSize.x,0 }))
                        {
                           
                            // Hit left
                            if (CheckCollisionCircleRec(ball.position, ball.radius,
                                { bricks[i][j].position.x - uses_bricks::brickSize.x / 2,
                                bricks[i][j].position.y - uses_bricks::brickSize.y / 2 ,
                                0,uses_bricks::brickSize.y }))
                            {

                                PlaySound(entities::uses_bricks::bricks_crack);
                                ball.speed.x *= -1;
                                ball.speed.y *= -1;
                                bricks[i][j].lifes--;
                                if (bricks[i][j].lifes == 0)
                                {
                                    bricks[i][j].active = false;
                                }
                                else
                                {
                                    switch (bricks[i][j].lifes)
                                    {
                                    case 1:bricks[i][j].state = 3;
                                        break;
                                    case 2:bricks[i][j].state = 2;
                                        break;
                                    case 3:bricks[i][j].state = 1;
                                    }
                                }
                            }
                            // Hit right
                            else if (CheckCollisionCircleRec(ball.position, ball.radius,
                                { bricks[i][j].position.x + uses_bricks::brickSize.x / 2,
                                bricks[i][j].position.y - uses_bricks::brickSize.y / 2 ,
                                0,uses_bricks::brickSize.y }))
                            {

                                PlaySound(entities::uses_bricks::bricks_crack);
                                ball.speed.x *= -1;
                                ball.speed.y *= -1;
                                bricks[i][j].lifes--;
                                if (bricks[i][j].lifes == 0)
                                {
                                    bricks[i][j].active = false;
                                }
                                else
                                {
                                    switch (bricks[i][j].lifes)
                                    {
                                    case 1:bricks[i][j].state = 3;
                                        break;
                                    case 2:bricks[i][j].state = 2;
                                        break;
                                    case 3:bricks[i][j].state = 1;
                                    }
                                }
                            }
                            else
                            {
                                PlaySound(entities::uses_bricks::bricks_crack);
                                ball.speed.y *= -1;
                                bricks[i][j].lifes--;
                                if (bricks[i][j].lifes == 0)
                                {
                                    bricks[i][j].active = false;
                                    score += 20;
                                }
                                else
                                {
                                    switch (bricks[i][j].lifes)
                                    {
                                    case 1:bricks[i][j].state = 3;
                                        break;
                                    case 2:bricks[i][j].state = 2;
                                        break;
                                    case 3:bricks[i][j].state = 1;
                                    }
                                }
                            }
                        }
                        // Hit left
                        else if (CheckCollisionCircleRec(ball.position, ball.radius,
                            { bricks[i][j].position.x - uses_bricks::brickSize.x / 2,
                            bricks[i][j].position.y - uses_bricks::brickSize.y / 2 ,
                            0,uses_bricks::brickSize.y }))
                        {
                           
                            PlaySound(entities::uses_bricks::bricks_crack);
                            ball.speed.x *= -1;
                            bricks[i][j].lifes--;
                            if (bricks[i][j].lifes == 0)
                            {
                                bricks[i][j].active = false;
                            }
                            else
                            {
                                switch (bricks[i][j].lifes)
                                {
                                case 1:bricks[i][j].state = 3;
                                    break;
                                case 2:bricks[i][j].state = 2;
                                    break;
                                case 3:bricks[i][j].state = 1;
                                }
                            }
                        }
                        // Hit right
                        else if (CheckCollisionCircleRec(ball.position, ball.radius,
                            { bricks[i][j].position.x + uses_bricks::brickSize.x / 2,
                            bricks[i][j].position.y - uses_bricks::brickSize.y / 2 ,
                            0,uses_bricks::brickSize.y }))
                        {
                           
                            PlaySound(entities::uses_bricks::bricks_crack);
                            ball.speed.x *= -1;
                            bricks[i][j].lifes--;
                            if (bricks[i][j].lifes == 0)
                            {
                                bricks[i][j].active = false;
                            }
                            else
                            {
                                switch (bricks[i][j].lifes)
                                {
                                case 1:bricks[i][j].state = 3;
                                    break;
                                case 2:bricks[i][j].state = 2;
                                    break;
                                case 3:bricks[i][j].state = 1;
                                }
                            }
                        }
                        else if (true)
    {

    }
                    }
                }
            }
        }
        void gameOver()
        {
            if (player.life <= 0) logic::gameOver = true;
            else
            {
                logic::gameOver = true;
                logic::win = true;
                for (int i = 0; i < LINES_OF_BRICKS; i++)
                {
                    for (int j = 0; j < BRICKS_PER_LINE; j++)
                    {
                        if (bricks[i][j].active)
                        {
                            logic::gameOver = false; 
                            logic::win = false;
                        }
                    }
                }
               
            }
        }
        void updateAnims()
        {
            //player anms.
            player.sourceRec.y = (float)player.state * (float)player.frameHeight;
            animatedTimer::player--;
            //ground.
            animatedTimer::ground--;
            if (animatedTimer::ground < 0)
            {
                animatedTimer::ground = FPSGROUND;
                GameplayLogic::backgroundAnimation(ground::groundState, 2); // 2 (cantidad de fotogramas - 1);
            }
            ground::groundSource.y = (float)ground::groundState * (float)ground::frameHightGround;
            //blocks.
            for (int i = 0; i < LINES_OF_BRICKS; i++)
            {
                for (int j = 0; j < BRICKS_PER_LINE; j++)
                {
                    if (!bricks[i][j].active)
                    {
                        if (bricks[i][j].self_Destruction>0 && bricks[i][j].self_Destruction<=TIMERBLOCKS)
                        {
                            bricks[i][j].self_Destruction--;
                            if (bricks[i][j].self_Destruction<50)
                            {
                                if (bricks[i][j].self_Destruction < 25)
                                {
                                    if (bricks[i][j].self_Destruction < 0)
                                    {
                                        //stop
                                    }
                                    else
                                    {
                                        bricks[i][j].state = 6;
                                    }
                                }
                                else
                                {
                                    bricks[i][j].state = 5;
                                }
                            }
                            else
                            {
                                bricks[i][j].state = 4;
                            }

                        }
                    }
                }
            }
        }
        void confirmWin()
        {
            if (logic::win)
            {
                if (gameplay::level<5)
                {
                    gameplay::level++;
                    totalscore += score;
                }
                else
                {
                    //game over.
                    gameplay::level = 0;
                    totalscore = 0;
                }
                
            }
        }
        void all()
        {
            SetMusicVolume(gameplay::entities::ground::sground,arkanoid::game::musicVolume);
            SetSoundVolume(gameplay::entities::ground::swalls, arkanoid::game::fxVolume);
            SetSoundVolume(entities::uses_bricks::bricks_crack, arkanoid::game::fxVolume);
            if (!logic::gameOver)
            {

                pressPause();
               
                if (!logic::pause)
                {
                    
                    playerMove();
                   
                    // Ball launching logic
                    hacks();
                    ballLauncher();

                    // Ball movement logic
                    BallMove();

                    // Collision logic: ball vs walls 
                    ball_wall();

                    // Collision logic: ball vs player
                    ball_player();

                    // Collision logic: ball vs bricks
                    ball_bricks();

                    // Game over logic
                    gameOver();

                    //update anims.
                    updateAnims();
                    confirmWin();
                }
                else
                {
                    hacks();
                    if (IsKeyPressed(KEY_G))
                    {
                        arkanoid::game::currentScene = arkanoid::game::Scene::Menu;
                    }
                }
            }
            else
            {
                if (IsKeyPressed(KEY_ENTER))
                {
                    if (gameplay::level==5)
                    {
                        gameplay::level = 0;
                    }
                    init::chargeAll(); //inicia de vuelta. podria pasar al siguente lvl.
                    logic::gameOver = false;
                }
                if (IsKeyPressed(KEY_G))
                {
                    arkanoid::game::currentScene = arkanoid::game::Scene::Menu;
                }
            }
        }
        
    }
}
