#ifndef GAMEPLAYLOGIC_H
#define GAMEPLAYLOGIC_H
#include "raylib.h"
#include "Scenes/gameplay.h"
#include "Scenes/both/posFuns.h"
#include "ExternV/niveles.h"
namespace GameplayLogic
{
    void backgroundAnimation(short& actualframe,short cantframes);
    namespace init
    {
        void chargeAll();
    }
    namespace update
    {
        void all();
    }
    
}


#endif