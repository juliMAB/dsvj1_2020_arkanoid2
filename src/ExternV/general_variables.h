#ifndef GENERAL_VARIABLES_H
#define GENERAL_VARIABLES_H
#define LINES_OF_BRICKS 6
#define BRICKS_PER_LINE 13
#include "raylib.h"
#include "controler/game.h"


namespace fps
{
	const int fps = 60;
}
namespace resolution
{
	
	const Vector2 possible_resolution[5] = 
	{
	{300,400},
	{500,667},
	{600,800},
	{700,934},
	{809,1800}
	};
}

#endif