#ifndef NIVELES_H
#define NIVELES_H
#include "general_variables.h"
namespace lvl
{
	const bool nivel[5][LINES_OF_BRICKS][BRICKS_PER_LINE] =
	{
		//1
		{
			{false, false, false, false, false, false, false, false, false, false, false, false, false },
			{false, false, false, false, false, false, false, false, false, false, false, false, false },
			{false, false, false, false, false, false, false, false, false, false, false, false, false },
			{false,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true, false },
			{false,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true,  true, false },
			{false, false, false, false, false, false, false, false, false, false, false, false, false },
		},
		//2
		{
			{true, false, false, true , false, false, true , false, false, true , false, false, true  },
			{true, false, false, true , false, false, true , false, false, true , false, false, true  },
			{true, false, false, true , false, false, true , false, false, true , false, false, true  },
			{true, false, false, true , false, false, true , false, false, true , false, false, true  },
			{true, false, false, true , false, false, true , false, false, true , false, false, true  },
			{true, false, false, true , false, false, true , false, false, true , false, false, true  },
		},
		//3
		{
			{true , false, false, false, false, false, false, false, false, false, false, false, false },
			{false, true , false, false, false, false, false, true , false, false, false, false, false },
			{false, true , true , false, false, false, true , false, true , false, false, false, true  },
			{false, true , false, true , false, true , false, false, false, true , false, true , false },
			{false, true , false, false, true , false, false, false, false, false, true , false, false },
			{false, false, false, false, false, false, false, false, false, false, false, false, false },
		},
		//4
		{
			{true , true , true , false, true , false, true , false, true , false, false, true , false },
			{false, false, true , false, true , false, true , false, true , false, false, false, false },
			{false, false, true , false, true , false, true , false, true , false, false, true , false },
			{true , false, true , false, true , false, true , false, true , false, false, true , false },
			{true , false, true , false, true , false, true	, false, true , false, false, true , false },
			{false, true , false, false, true , true , true , false, true , true , false, true , false },
		},
		//5
		{
			{true , false, true , false, true , false, true , false, true , false, true , false, true  },
			{false, true , false, true , false, true , false, true , false, true , false, true , false },
			{true , false, true , false, true , false, true , false, true , false, true , false, true  },
			{false, true , false, true , false, true , false, true , false, true , false, true , false },
			{true , false, true , false, true , false, true , false, true , false, true , false, true  },
			{false, true , false, true , false, true , false, true , false, true , false, true , false },
		},
	};
}


#endif
