#ifndef ENTITIES_GAMEPLAY_H
#define ENTITIES_GAMEPLAY_H

#include "raylib.h"


	struct Ball
	{
		Vector2 position;
		Vector2 speed;
		float radius;
		Color color;
		bool active;
		Texture2D texture;
		
	};
	struct Player {
		int life;
		Texture2D texture;
		int frameHeight;
		Rectangle sourceRec;									//toma las dimensiones del archivo de textura.
		Rectangle bounds;										//limites.
		short state;											//estados.
		Sound sound;
	};
	struct Brick
	{
		Vector2 position;
		bool active;
		Color color;
		short self_Destruction;
		short state;
		short lifes;
	};
	struct PowerUp
	{
		short timer;
		short lifes;
		//powers name;
	};
	enum class powers
	{
		big,
		stiky,
		multi,
		shoot
	};

#endif