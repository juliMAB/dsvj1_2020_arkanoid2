#include "game.h"

namespace arkanoid
{
	namespace game
	{
		
		
		Scene currentScene;

		bool testing=false;

		bool gameOver;

		short NroResolution;

		float fxVolume;

		float musicVolume;

		bool changeResolution = false;

		Vector2 resolution;
		void run()
		{
			init();

			while (!gameOver)
			{
				if (changeResolution)
				{
					menu::init();
					gameplay::init();
					changeResolution = false;
				}
				update();
				draw();
			}
			deinit();
			if (gameOver)  WindowShouldClose();
			
		}

		static void init()
		{
			
			if (!changeResolution)
			{
				InitWindow(600, 800, "ARKANOID 1.0");
			}
			
			InitAudioDevice();
			SetTargetFPS(fps::fps);
#if TESTING
			testing = true;
#endif
			gameOver = false;
			NroResolution = 2;
			fxVolume = VOLUME_GENERAL;
			musicVolume = VOLUME_GENERAL;
			menu::init();
			gameplay::init();
		}

		static void update()
		{
			
			switch (currentScene)
			{
			case Scene::Menu:
				menu::update();
				break;
			case Scene::Game:
				gameplay::update();
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			
			switch (currentScene)
			{
			case Scene::Menu:
				menu::draw();
				break;
			case Scene::Game:
				gameplay::draw();
				break;
			}
			if (testing)
			{
				draw::showFps();
				draw::showCurrentScene();
				draw::showResolution();
			}
			
			EndDrawing();
		}

		static void deinit()
		{
			CloseWindow();

			menu::deinit();
			gameplay::deinit();
		}
	}
}