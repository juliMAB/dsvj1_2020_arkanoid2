#ifndef GAME_H
#define GAME_H

#include "raylib.h"

#include "Scenes/gameplay.h"
#include "Scenes/menu.h"
#include "ExternV/general_variables.h"
#include "ExternV/Scenes.h"
#include "Scenes/both/draw.h"



#define VOLUME_GENERAL 0.10f
namespace arkanoid
{

	namespace game
	{
		
		enum class Scene
		{
			Menu,
			Game
		};

		extern Scene currentScene;
		extern bool testing;
		extern bool gameOver;
		extern float fxVolume;
		extern float musicVolume;
		extern short NroResolution;
		extern Vector2 resolution;
		extern bool changeResolution;
		
		void run();

		static void init();
		static void update();
		static void draw();
		static void deinit();
	}
}

#endif